<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ShippingBundle\Zones;

use Doctrine\ORM\EntityManager;

/**
 * Description of Dpd
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class UK extends AbstractZones
{

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * Search for data
     * Set postcode and weight before running this method
     * 
     */
    protected function doSearch()
    {

        $regex = $this->getRegex();
        $numSubString = $this->getSubString();
        $substr = substr($this->postcode, 0, 2);

        if (preg_match($regex, $this->postcode) && (
                ($substr == "AB" && (in_array($numSubString, range(31, 38)) || in_array($numSubString, range(40, 56)))) ||
                ($substr == "IV" && (in_array($numSubString, range(1, 28)) || in_array($numSubString, range(30, 32)) || in_array($numSubString, range(36, 40)) || in_array($numSubString, range(52, 54)))) ||
                ($substr == "IV" && $numSubString == 63) ||
                ($substr == "KW" && in_array($numSubString, range(1, 14))) ||
                ($substr == "PA" && in_array($numSubString, range(21, 38))) ||
                ($substr == "PH" && in_array($numSubString, range(4, 41))) ||
                ($substr == "PH" && ($numSubString == 49 || $numSubString == 50)) ||
                ($substr == "IV" && (in_array($numSubString, range(41, 49)) || $numSubString == 51 || $numSubString == 55 || $numSubString == 56 )) ||
                ($substr == "KA" && ($numSubString == 27 || $numSubString == 28)) ||
                ($substr == "KW" && in_array($numSubString, range(15, 17))) ||
                ($substr == "PA" && $numSubString == 20) ||
                ($substr == "PA" && in_array($numSubString, range(41, 49))) ||
                ($substr == "PA" && in_array($numSubString, range(60, 78))) ||
                ($substr == "PH" && in_array($numSubString, range(42, 44)))
                )) {
            $this->setZone("C");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "BT")
                )) {
            $this->setZone("BT");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "IM")
                )) {
            $this->setZone("IM");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "JE")
                )) {
            $this->setZone("JE");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "GY")
                )) {
            $this->setZone("GY");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "ZE")
                )) {
            $this->setZone("ZE");
        }
        elseif (preg_match($regex, $this->postcode) && (
                ($substr == "HS")
                )) {
            $this->setZone("HS");
        }
        else {
            $this->setZone("A");
        }
    }

}
