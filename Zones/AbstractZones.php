<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ShippingBundle\Zones;

use Doctrine\ORM\EntityManager;

/**
 * Description of AbstractZones
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
abstract class AbstractZones
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $desc;

    /**
     * @var char
     */
    protected $zone;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var string 
     */
    protected $postcode;

    /**
     * @var float 
     */
    protected $weight;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    protected function setPrice($price)
    {
        $postcode = strtoupper(preg_replace('/\s+/', '', $postcode));
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    protected function setDesc($desc)
    {
        $this->desc = $desc;
    }

    /**
     * Returns zone of postcode
     *
     * @return string
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param string $zone
     */
    protected function setZone($zone)
    {
        $this->zone = $zone;
    }

    /**
     * Returns postcode regex
     *
     * @return string
     */
    protected function getRegex()
    {
        return '#^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$#';
    }

    /**
     * @return string
     */
    protected function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = strtoupper($postcode);
        $this->doSearch();
    }

    abstract protected function doSearch();

    /**
     * @return float
     */
    protected function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     *
     * @return int
     */
    protected function getSubString()
    {
        $num = preg_match_all('/[0-9]/', $this->postcode);

        if ($num == 2) {
            $numsubstr = substr($this->postcode, 2, 1);
        }
        else {
            $numsubstr = substr($this->postcode, 2, 2);
        }

        return $numsubstr;
    }

    protected function getData()
    {
        if (null === $this->weight || !is_float($this->weight)) {
            throw new \RuntimeException(sprintf("The weight should be a float but is %s instead", gettype($this->weight)));
        }
    }
}
