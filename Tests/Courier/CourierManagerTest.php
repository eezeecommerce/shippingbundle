<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 03/06/2016
 * Time: 11:34
 */

namespace eezeecommerce\ShippingBundle\Tests\Courier;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use eezeecommerce\ShippingBundle\Courier\CourierManager;
use eezeecommerce\ShippingBundle\Courier\PriceManager;
use eezeecommerce\ShippingBundle\Entity\Country;
use eezeecommerce\ShippingBundle\Entity\CourierServicePricing;
use eezeecommerce\ShippingBundle\Entity\CourierServices;
use eezeecommerce\ShippingBundle\Zones\UK;

class CourierManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructerAcceptsArgs()
    {
        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $this->assertNotFalse($result);

    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Country code should be a 2 character string instead it was set to  which is type of NULL
     */
    public function testNoCurrencyCodeThrowsException()
    {
        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $result->setCountryCode(null);

        $result->getResults();
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Weight should be a signed integer instead it was set to  which is type of NULL
     */
    public function testNullWeightThrowsException()
    {
        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $result->setCountryCode("GB");
        $result->setWeight(null);

        $result->getResults();
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Weight should be a signed integer instead it was set to -1 which is type of integer
     */
    public function testNegativeWeightThrowsException()
    {
        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $result->setCountryCode("GB");
        $result->setWeight(-1);

        $result->getResults();
    }

    public function testNullResultsReturnsNull()
    {

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->setMethods(array("findOneByCode", "findByWeightCountryCode"))
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->once())
            ->method("findOneByCode")
            ->will($this->returnValue(null));

        $repository->expects($this->once())
            ->method("findByWeightCountryCode")
            ->will($this->returnValue(null));

        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entitymanager->expects($this->any())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $result->setCountryCode("GB");
        $result->setWeight(1);

        $this->assertNull($result->getResults());


    }

    public function testEmptyResultsReturnsEmptyArray()
    {
        $array = new ArrayCollection();

        $repo = $this->getMockBuilder(CourierServicePricing::class)
            ->disableOriginalConstructor()
            ->getMock();

        $courierservice = $this->getMockBuilder(CourierServices::class)
            ->disableOriginalConstructor()
            ->getMock();

        $courierservice->expects($this->any())
            ->method("getEnableOrdertotalCutoffs")
            ->will($this->returnValue(0));

        $repo->expects($this->any())
            ->method("getCourierService")
            ->will($this->returnValue($courierservice));

        $array->add($repo);

        $repository = $this->getMockBuilder(EntityRepository::class)
            ->setMethods(array("findOneByCode", "findByWeightCountryCode"))
            ->disableOriginalConstructor()
            ->getMock();

        $repository->expects($this->any())
            ->method("findOneByCode")
            ->will($this->returnValue(null));

        $repository->expects($this->any())
            ->method("findByWeightCountryCode")
            ->will($this->returnValue($array));

        $entitymanager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entitymanager->expects($this->any())
            ->method("getRepository")
            ->will($this->returnValue($repository));

        $uk = $this->getMockBuilder(UK::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pricemanager = $this->getMockBuilder(PriceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $result = new CourierManager($entitymanager, $uk, $pricemanager);

        $result->setCountryCode("GB");
        $result->setWeight(1);
        $this->assertEquals($result->getResults(), ["0" => null]);


    }
}
