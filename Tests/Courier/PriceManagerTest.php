<?php
/**
 * Created by PhpStorm.
 * User: dan
 * Date: 03/06/2016
 * Time: 15:18
 */

namespace eezeecommerce\ShippingBundle\Tests\Courier;


use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\ShippingBundle\Courier\PriceManager;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\ShippingBundle\Entity\Courier;
use eezeecommerce\ShippingBundle\Entity\CourierServicePricing;
use eezeecommerce\ShippingBundle\Entity\CourierServices;

class PriceManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testSetCurrencyAcceptsArgs()
    {
        $currencymanager = $this->getMockBuilder(CurrencyManager::class)
            ->setMethods(["getEntity", "get"])
            ->disableOriginalConstructor()
            ->getMock();

        $currencyitem = $this->getMockBuilder(CurrencyItem::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currency = $this->getMockBuilder(Currency::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencymanager->expects($this->any())
            ->method("get")
            ->will($this->returnValue($currencyitem));

        $currencymanager->expects($this->any())
            ->method("getEntity")
            ->will($this->returnValue($currency));

        $pm = new PriceManager();

        $result = $pm->setCurrency($currencymanager);

        $this->assertNotFalse($result);
    }

    public function testGetPriceAcceptsArgs()
    {
        $courierservicepricing = $this->getMockBuilder(CourierServicePricing::class)
            ->disableOriginalConstructor()
            ->getMock();

        $weight = 1;

        $courierservicepricing->expects($this->once())
            ->method("getId")
            ->will($this->returnValue("1"));

        $courierservice = $this->getMockBuilder(CourierServices::class)
            ->setMethods(["getName", "getMinDelivery", "getMaxDelivery", "getCourier"])
            ->disableOriginalConstructor()
            ->getMock();

        $courierservicepricing->expects($this->any())
            ->method("getCourierService")
            ->will($this->returnValue($courierservice));

        $courierservice->expects($this->once())
            ->method("getMinDelivery")
            ->will($this->returnValue("1"));

        $courierservice->expects($this->once())
            ->method("getMaxDelivery")
            ->will($this->returnValue("2"));

        $courierservice->expects($this->once())
            ->method("getName")
            ->will($this->returnValue("Courier Service"));

        $courier = $this->getMockBuilder(Courier::class)
            ->setMethods(["getName"])
            ->disableOriginalConstructor()
            ->getMock();

        $courierservice->expects($this->once())
            ->method("getCourier")
            ->will($this->returnValue($courier));

        $courier->expects($this->once())
            ->method("getName")
            ->will($this->returnValue("Courier Name"));


        $courierservice->expects($this->once())
            ->method("getBasePrice")
            ->will($this->returnValue(12.99));


        $result = new PriceManager();

        $result->getPrice($courierservicepricing, $weight);
    }
}
