<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ShippingBundle\Courier;

use Doctrine\ORM\EntityManager;
use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use eezeecommerce\ShippingBundle\Entity\CourierServicePricing;
use eezeecommerce\ShippingBundle\Zones\UK;
use eezeecommerce\ShippingBundle\Courier\PriceManager;

/**
 * Description of getCourier
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class CourierManager
{

    private $em;
    private $weight;
    private $postcode;
    private $countryCode;
    private $total;

    /**
     * @var \eezeecommerce\ShippingBundle\Zones\UK
     */
    private $variableZones;

    public function __construct(EntityManager $em, UK $variableZones, PriceManager $priceManager)
    {
        $this->em = $em;
        $this->variableZones = $variableZones;
        $this->priceManager = $priceManager;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        $this->variableZones->setWeight($weight);
    }

    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
        $this->variableZones->setPostcode($postcode);
    }

    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getResults()
    {
        if (null === $this->countryCode) {
            throw new \InvalidArgumentException(sprintf("Country code should be a 2 character string instead it was set to %s which is type of %s", $this->countryCode, gettype($this->countryCode)));
        }

        if (null === $this->weight || $this->weight < 0) {
            throw new \InvalidArgumentException(sprintf("Weight should be a signed integer instead it was set to %s which is type of %s", $this->weight, gettype($this->weight)));
        }

        $country = $this->em->getRepository("eezeecommerceShippingBundle:Country")
                ->findOneByCode($this->countryCode);

        $results = $this->em->getRepository("eezeecommerceShippingBundle:CourierServicePricing")
                ->findByWeightCountryCode($country, $this->weight);

        if (count($results) < 1) {
            return null;
        }

        $list = array();


        foreach ($results as $result) {

            if (false == $result->getCourierService()->getEnableOrdertotalCutoffs()) {
                if ($this->countryCode == "GB") {
                    $check = $this->checkZone($result);
                    if (!$check) {
                        $list[] = $this->priceManager->getPrice($result, $this->weight);
                    } else {
                        if ($result->getCourierService()->getZone() === $this->variableZones->getZone()) {
                            $list[] = $this->priceManager->getPrice($result, $this->weight);
                        }
                    }
                } else {
                    $list[] = $this->priceManager->getPrice($result, $this->weight);
                }
            } else {
                $min = $result->getCourierService()->getMinOrdertotal() ? : 0;
                $max = $result->getCourierService()->getMaxOrdertotal() ? : 2147483647;
                if ($min <= $this->total && $max >= $this->total) {
                    if ($this->countryCode == "GB") {
                        $check = $this->checkZone($result);
                        if (!$check) {
                            $list[] = $this->priceManager->getPrice($result, $this->weight);
                        } else {
                            if ($result->getCourierService()->getZone() === $this->variableZones->getZone()) {
                                $list[] = $this->priceManager->getPrice($result, $this->weight);
                            }
                        }
                    } else {
                        $list[] = $this->priceManager->getPrice($result, $this->weight);
                    }
                }
            }
        }
        return $list;
    }

    private function checkZone(CourierServicePricing $courierServicePricing)
    {
        return ($courierServicePricing->getCourierService()->getVariableZones() == 1);
    }

}
