<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ShippingBundle\Courier;

use eezeecommerce\CurrencyBundle\Core\CurrencyManager;
use eezeecommerce\CurrencyBundle\Entity\Currency;
use eezeecommerce\ShippingBundle\Entity\CourierServicePricing;

/**
 * Description of PriceManager
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class PriceManager
{
    /**
     * @var Currency
     */
    protected $currency;

    public function setCurrency(CurrencyManager $currency)
    {
        $this->currency = $currency->get()->getEntity();
    }

    public function getPrice(CourierServicePricing $courierServicePricing, $weight)
    {
        $result = array(
            "id" => $courierServicePricing->getId(),
            "courierService" => array(
                "minDelivery" => $courierServicePricing->getCourierService()->getMinDelivery(),
                "maxDelivery" => $courierServicePricing->getCourierService()->getMaxDelivery(),
                "name" => $courierServicePricing->getCourierService()->getName(),
                "courier" => array(
                    "name" => $courierServicePricing->getCourierService()->getCourier()->getName()
                )
            ),
        );

        $price = $courierServicePricing->getBasePrice();
        
        if (($weight > $courierServicePricing->getMaxWeight()) && ($courierServicePricing->getCourierService()->getAfterWeight() > 0 && $courierServicePricing->getCourierService()->getAfterWeight() > 0)
        ) {
            $additionalWeightMultiple = ceil(($weight - $courierServicePricing->getMaxWeight()) / $courierServicePricing->getCourierService()->getAfterWeight());
            $price = $courierServicePricing->getBasePrice() + ($courierServicePricing->getCourierService()->getAfterBasePrice() * $additionalWeightMultiple);
        }

        $result["price"] = $price * $this->currency->getExchangeRate();
        
        return $result;
    }
}
