<?php

namespace eezeecommerce\ShippingBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use eezeecommerce\ShippingBundle\Entity\Courier;

class LoadCourierData implements OrderedFixtureInterface, FixtureInterface
{
    public function getOrder()
    {
        return 49;
    }

    public function load(ObjectManager $manager)
    {
        $courier = new Courier();
        
        $courier->setName("Royal Mail");
        $courier->setPhone("0345 774 0740");
        $courier->setTrackingUri("http://www.royalmail.com/portal/rm/track?trackNumber=");
        
        $manager->persist($courier);
        
        $courier = new Courier();
        
        $courier->setName("DPD");
        $courier->setPhone("0844 556 0560");
        $courier->setTrackingUri("http://www.dpd.co.uk/apps/tracking/?parcel=");
        
        $manager->persist($courier);
        
        $manager->flush();
    }

}
