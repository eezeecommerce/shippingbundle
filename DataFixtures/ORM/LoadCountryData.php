<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\ShippingBundle\DataFixtures\ORM;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use eezeecommerce\ShippingBundle\Entity\Country;
use RuntimeException;

class LoadCountryData implements OrderedFixtureInterface, FixtureInterface
{
    public function getOrder()
    {
        return 50;
    }
    
    public function load(ObjectManager $manager)
    {
        if (($handle = fopen(__DIR__ . "/../../Resources/data/countries.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                list($name, $currencyCode ,$code, $iso3) = $data;
                $country = new Country();
                $country->setCode($code);
                $country->setName($name);
                $country->setIso3($iso3);
                $country->setCurrencyCode($currencyCode);
                $manager->persist($country);
            }
            fclose($handle);
        } else {
            throw new RuntimeException('Failed to parse countries');
        }
        $manager->flush();
    }

}
