<?php

namespace eezeecommerce\ShippingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\ShippingBundle\Form\CourierServicePricingType;
use Doctrine\ORM\EntityRepository;

class CourierServicesType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('variable_zones')
                ->add('zone')
                ->add('courier', 'entity', array(
                    'class' => "eezeecommerceShippingBundle:Courier",
                    'property' => "name"
                ))
                ->add('country', 'entity', array(
                    'class' => "eezeecommerceShippingBundle:Country",
                    'query_builder' => function(EntityRepository $em) {
                        return $em->createQueryBuilder('u')
                                ->orderBy('u.sort', 'ASC')
                                ->orderBy('u.name', 'ASC');
                    },
                    'property' => "name",
                    "multiple" => true,
                    "label" => false
                ))
                ->add('min_delivery')
                ->add('max_delivery')
                ->add('service_pricing', "collection", array(
                    "label" => false,
                    "type" => new CourierServicePricingType(),
                    "required" => false,
                    "allow_add" => true,
                    "allow_delete" => true,
                    "prototype" => true,
                    "by_reference" => false,
                    "prototype_name" => "__courier_services_price__"
                ))
                ->add('after_weight')
                ->add('after_base_price')
                ->add('uses_vol_weight')
                ->add('divisable_vol_value')
                ->add('enable_ordertotal_cutoffs')
                ->add('min_ordertotal', null, array(
                    "label" => "Minimum Order Total"
                ))
                ->add('max_ordertotal', null, array(
                    "label" => "Maximum Order Total"
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ShippingBundle\Entity\CourierServices'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_shippingbundle_courierservices';
    }

}
