<?php

namespace eezeecommerce\ShippingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CourierServicePricingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('min_weight')
            ->add('max_weight')
            ->add('base_price')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ShippingBundle\Entity\CourierServicePricing'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_shippingbundle_courierservicepricing';
    }
}
