<?php

namespace eezeecommerce\ShippingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="eezeecommerce\ShippingBundle\Entity\CountryRepository")
 */
class Country
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column(name="code", type="string")
     */
    protected $code;
    
    /**
     * @ORM\Column(name="name", type="string", unique=true)
     */
    protected $name;
    
    /**
     * @ORM\Column(name="iso3", type="string")
     */
    protected $iso3;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $sort;
    
    /**
     * @ORM\Column(name="currency_code", type="string")
     */
    protected $currencyCode;
    
    /**
     * @ORM\ManyToMany(targetEntity="CourierServices", mappedBy="country")
     */
    private $shipping_service;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\TaxBundle\Entity\TaxRates", inversedBy="country")
     * @ORM\JoinColumn(name="taxrate_id", referencedColumnName="id")
     */
    private $tax_rate = null;

    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\SettingsBundle\Entity\Settings", mappedBy="business_address_country")
     */
    private $settings = null;

    public function __construct()
    {
        $this->shipping_service = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iso3
     *
     * @param string $iso3
     *
     * @return Country
     */
    public function setIso3($iso3)
    {
        $this->iso3 = $iso3;

        return $this;
    }

    /**
     * Get iso3
     *
     * @return string
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     *
     * @return Country
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Add shippingService
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServices $shippingService
     *
     * @return Country
     */
    public function addShippingService(\eezeecommerce\ShippingBundle\Entity\CourierServices $shippingService)
    {
        $this->shipping_service[] = $shippingService;

        $shippingService->addCountry($this);
        
        return $this;
    }

    /**
     * Remove shippingService
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServices $shippingService
     */
    public function removeShippingService(\eezeecommerce\ShippingBundle\Entity\CourierServices $shippingService)
    {
        $this->shipping_service->removeElement($shippingService);
    }

    /**
     * Get shippingService
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippingService()
    {
        return $this->shipping_service;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Country
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set taxRate
     *
     * @param \eezeecommerce\TaxBundle\Entity\TaxRates $taxRate
     *
     * @return Country
     */
    public function setTaxRate(\eezeecommerce\TaxBundle\Entity\TaxRates $taxRate = null)
    {
        $this->tax_rate = $taxRate;
        
        return $this;
    }

    /**
     * Get taxRate
     *
     * @return \eezeecommerce\TaxBundle\Entity\TaxRates
     */
    public function getTaxRate()
    {
        return $this->tax_rate;
    }

    /**
     * Add setting
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $setting
     *
     * @return Country
     */
    public function addSetting(\eezeecommerce\SettingsBundle\Entity\Settings $setting)
    {
        $this->settings[] = $setting;

        $setting->setBusinessAddressCountry($this);

        return $this;
    }

    /**
     * Remove setting
     *
     * @param \eezeecommerce\SettingsBundle\Entity\Settings $setting
     */
    public function removeSetting(\eezeecommerce\SettingsBundle\Entity\Settings $setting)
    {
        $this->settings->removeElement($setting);
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettings()
    {
        return $this->settings;
    }
}
