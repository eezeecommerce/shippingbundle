<?php

namespace eezeecommerce\ShippingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="courier_service_pricing")
 * @ORM\Entity(repositoryClass="eezeecommerce\ShippingBundle\Entity\CourierServicePricingRepository")
 */
class CourierServicePricing
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="CourierServices", inversedBy="service_pricing")
     * @ORM\JoinColumn(name="courier_service_id", referencedColumnName="id")
     */
    private $courier_service;
    
    /**
     * @ORM\Column(name="min_weight", type="decimal", precision=19, scale=4)
     */
    private $min_weight;
    
    /**
     * @ORM\Column(name="max_weight", type="decimal", precision=19, scale=4)
     */
    private $max_weight;
    
    /**
     * @ORM\Column(name="base_price", type="decimal", precision=19, scale=4)
     */
    private $base_price;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set minWeight
     *
     * @param float $minWeight
     *
     * @return CourierServicePricing
     */
    public function setMinWeight($minWeight)
    {
        $this->min_weight = $minWeight;

        return $this;
    }

    /**
     * Get minWeight
     *
     * @return float
     */
    public function getMinWeight()
    {
        return $this->min_weight;
    }

    /**
     * Set maxWeight
     *
     * @param float $maxWeight
     *
     * @return CourierServicePricing
     */
    public function setMaxWeight($maxWeight)
    {
        $this->max_weight = $maxWeight;

        return $this;
    }

    /**
     * Get maxWeight
     *
     * @return float
     */
    public function getMaxWeight()
    {
        return $this->max_weight;
    }

    /**
     * Set basePrice
     *
     * @param float $basePrice
     *
     * @return CourierServicePricing
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * Get basePrice
     *
     * @return float
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Set courierService
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServices $courierService
     *
     * @return CourierServicePricing
     */
    public function setCourierService(\eezeecommerce\ShippingBundle\Entity\CourierServices $courierService = null)
    {
        $this->courier_service = $courierService;

        return $this;
    }

    /**
     * Get courierService
     *
     * @return \eezeecommerce\ShippingBundle\Entity\CourierServices
     */
    public function getCourierService()
    {
        return $this->courier_service;
    }

}
