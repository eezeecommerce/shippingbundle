<?php

namespace eezeecommerce\ShippingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="courier")
 * @ORM\Entity(repositoryClass="eezeecommerce\ShippingBundle\Entity\CourierRepository")
 */
class Courier
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    
    /**
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;
    
    /**
     * @ORM\Column(name="tracking_uri", type="string")
     */
    private $trackingUri;
    
    /**
     * @ORM\OneToMany(targetEntity="CourierServices", mappedBy="courier")
     */
    private $services;
    
    
    public function __construct()
    {
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Courier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Courier
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set trackingUri
     *
     * @param string $trackingUri
     *
     * @return Courier
     */
    public function setTrackingUri($trackingUri)
    {
        $this->trackingUri = $trackingUri;

        return $this;
    }

    /**
     * Get trackingUri
     *
     * @return string
     */
    public function getTrackingUri()
    {
        return $this->trackingUri;
    }

    /**
     * Add service
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServices $service
     *
     * @return Courier
     */
    public function addService(\eezeecommerce\ShippingBundle\Entity\CourierServices $service)
    {
        $this->services[] = $service;
        
        $service->setCourier($this);

        return $this;
    }

    /**
     * Remove service
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServices $service
     */
    public function removeService(\eezeecommerce\ShippingBundle\Entity\CourierServices $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set variableZones
     *
     * @param boolean $variableZones
     *
     * @return Courier
     */
    public function setVariableZones($variableZones)
    {
        $this->variable_zones = $variableZones;

        return $this;
    }

    /**
     * Get variableZones
     *
     * @return boolean
     */
    public function getVariableZones()
    {
        return $this->variable_zones;
    }

    /**
     * Set usesVolWeight
     *
     * @param boolean $usesVolWeight
     *
     * @return Courier
     */
    public function setUsesVolWeight($usesVolWeight)
    {
        $this->uses_vol_weight = $usesVolWeight;

        return $this;
    }

    /**
     * Get usesVolWeight
     *
     * @return boolean
     */
    public function getUsesVolWeight()
    {
        return $this->uses_vol_weight;
    }

    /**
     * Set divisableVolValue
     *
     * @param integer $divisableVolValue
     *
     * @return Courier
     */
    public function setDivisableVolValue($divisableVolValue)
    {
        $this->divisable_vol_value = $divisableVolValue;

        return $this;
    }

    /**
     * Get divisableVolValue
     *
     * @return integer
     */
    public function getDivisableVolValue()
    {
        return $this->divisable_vol_value;
    }
}
