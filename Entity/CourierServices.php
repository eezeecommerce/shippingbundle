<?php

namespace eezeecommerce\ShippingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="courier_services")
 * @ORM\Entity(repositoryClass="eezeecommerce\ShippingBundle\Entity\CourierServiceRepository")
 */
class CourierServices
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="Courier", inversedBy="services")
     * @ORM\JoinColumn(name="courier_id", referencedColumnName="id")
     */
    private $courier;

    /**
     * @ORM\OneToMany(targetEntity="CourierServicePricing", mappedBy="courier_service", cascade={"persist"})
     */
    private $service_pricing;
    
    /**
     * @ORM\Column(name="variable_zones", type="boolean")
     */
    private $variable_zones;
    
    /**
     * @ORM\Column(name="zone", type="string", nullable=true)
     */
    private $zone;
    
    /**
     * @ORM\ManyToMany(targetEntity="Country", inversedBy="shipping_service")
     * @ORM\JoinTable(name="courierservice_countries")
     */
    private $country;
    
    /**
     * @ORM\Column(name="min_delivery", type="integer")
     */
    private $min_delivery;
    
    /**
     * @ORM\Column(name="max_delivery", type="integer")
     */
    private $max_delivery;
    
    /**
     * @ORM\Column(name="after_weight", type="decimal", precision=19, scale=4)
     */
    private $after_weight = 0;
    
    /**
     * @ORM\Column(name="after_base_price", type="decimal", precision=19, scale=4)
     */
    private $after_base_price = 0;
    
    /**
     * @ORM\Column(name="uses_vol_weight", type="boolean")
     */
    private $uses_vol_weight;
    
    /**
     * @ORM\Column(name="divisable_vol_value", type="integer")
     */
    private $divisable_vol_value;
    
    /**
     * @ORM\Column(name="enable_ordertotal_cutoffs", type="boolean")
     */
    private $enable_ordertotal_cutoffs = false;
    
    /**
     * @ORM\Column(name="min_ordertotal", type="integer", nullable=true)
     */
    private $min_ordertotal;
    
    /**
     * @ORM\Column(name="max_ordertotal", type="integer", nullable=true)
     */
    private $max_ordertotal;
    
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->service_pricing = new ArrayCollection();
        $this->country = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CourierServices
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zone
     *
     * @param string $zone
     *
     * @return CourierServices
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set courier
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Courier $courier
     *
     * @return CourierServices
     */
    public function setCourier(\eezeecommerce\ShippingBundle\Entity\Courier $courier = null)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return \eezeecommerce\ShippingBundle\Entity\Courier
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Add servicePricing
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServicePricing $servicePricing
     *
     * @return CourierServices
     */
    public function addServicePricing(\eezeecommerce\ShippingBundle\Entity\CourierServicePricing $servicePricing)
    {
        $this->service_pricing[] = $servicePricing;
        
        $servicePricing->setCourierService($this);

        return $this;
    }

    /**
     * Remove servicePricing
     *
     * @param \eezeecommerce\ShippingBundle\Entity\CourierServicePricing $servicePricing
     */
    public function removeServicePricing(\eezeecommerce\ShippingBundle\Entity\CourierServicePricing $servicePricing)
    {
        $this->service_pricing->removeElement($servicePricing);
    }

    /**
     * Get servicePricing
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServicePricing()
    {
        return $this->service_pricing;
    }

    /**
     * Add country
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $country
     *
     * @return CourierServices
     */
    public function addCountry(\eezeecommerce\ShippingBundle\Entity\Country $country)
    {
        $this->country[] = $country;
        
        $country->addShippingService($this);

        return $this;
    }

    /**
     * Remove country
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $country
     */
    public function removeCountry(\eezeecommerce\ShippingBundle\Entity\Country $country)
    {
        $this->country->removeElement($country);
    }

    /**
     * Get country
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set variableZones
     *
     * @param boolean $variableZones
     *
     * @return CourierServices
     */
    public function setVariableZones($variableZones)
    {
        $this->variable_zones = $variableZones;

        return $this;
    }

    /**
     * Get variableZones
     *
     * @return boolean
     */
    public function getVariableZones()
    {
        return $this->variable_zones;
    }

    /**
     * Set minDelivery
     *
     * @param integer $minDelivery
     *
     * @return CourierServices
     */
    public function setMinDelivery($minDelivery)
    {
        $this->min_delivery = $minDelivery;

        return $this;
    }

    /**
     * Get minDelivery
     *
     * @return integer
     */
    public function getMinDelivery()
    {
        return $this->min_delivery;
    }

    /**
     * Set maxDelivery
     *
     * @param integer $maxDelivery
     *
     * @return CourierServices
     */
    public function setMaxDelivery($maxDelivery)
    {
        $this->max_delivery = $maxDelivery;

        return $this;
    }

    /**
     * Get maxDelivery
     *
     * @return integer
     */
    public function getMaxDelivery()
    {
        return $this->max_delivery;
    }

    /**
     * Set afterWeight
     *
     * @param string $afterWeight
     *
     * @return CourierServices
     */
    public function setAfterWeight($afterWeight)
    {
        $this->after_weight = $afterWeight;

        return $this;
    }

    /**
     * Get afterWeight
     *
     * @return string
     */
    public function getAfterWeight()
    {
        return $this->after_weight;
    }

    /**
     * Set afterBasePrice
     *
     * @param string $afterBasePrice
     *
     * @return CourierServices
     */
    public function setAfterBasePrice($afterBasePrice)
    {
        $this->after_base_price = $afterBasePrice;

        return $this;
    }

    /**
     * Get afterBasePrice
     *
     * @return string
     */
    public function getAfterBasePrice()
    {
        return $this->after_base_price;
    }

    /**
     * Set usesVolWeight
     *
     * @param boolean $usesVolWeight
     *
     * @return CourierServices
     */
    public function setUsesVolWeight($usesVolWeight)
    {
        $this->uses_vol_weight = $usesVolWeight;

        return $this;
    }

    /**
     * Get usesVolWeight
     *
     * @return boolean
     */
    public function getUsesVolWeight()
    {
        return $this->uses_vol_weight;
    }

    /**
     * Set divisableVolValue
     *
     * @param integer $divisableVolValue
     *
     * @return CourierServices
     */
    public function setDivisableVolValue($divisableVolValue)
    {
        $this->divisable_vol_value = $divisableVolValue;

        return $this;
    }

    /**
     * Get divisableVolValue
     *
     * @return integer
     */
    public function getDivisableVolValue()
    {
        return $this->divisable_vol_value;
    }

    /**
     * Set enableOrdertotalCutoffs
     *
     * @param boolean $enableOrdertotalCutoffs
     *
     * @return CourierServices
     */
    public function setEnableOrdertotalCutoffs($enableOrdertotalCutoffs)
    {
        $this->enable_ordertotal_cutoffs = $enableOrdertotalCutoffs;

        return $this;
    }

    /**
     * Get enableOrdertotalCutoffs
     *
     * @return boolean
     */
    public function getEnableOrdertotalCutoffs()
    {
        return $this->enable_ordertotal_cutoffs;
    }

    /**
     * Set minOrdertotal
     *
     * @param integer $minOrdertotal
     *
     * @return CourierServices
     */
    public function setMinOrdertotal($minOrdertotal)
    {
        $this->min_ordertotal = $minOrdertotal;

        return $this;
    }

    /**
     * Get minOrdertotal
     *
     * @return integer
     */
    public function getMinOrdertotal()
    {
        return $this->min_ordertotal;
    }

    /**
     * Set maxOrdertotal
     *
     * @param integer $maxOrdertotal
     *
     * @return CourierServices
     */
    public function setMaxOrdertotal($maxOrdertotal)
    {
        $this->max_ordertotal = $maxOrdertotal;

        return $this;
    }

    /**
     * Get maxOrdertotal
     *
     * @return integer
     */
    public function getMaxOrdertotal()
    {
        return $this->max_ordertotal;
    }
}
